package hf20200515swingwithmvc.View;

import hf20200515swingwithmvc.Controller.Controller;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class SwingView extends JFrame {
    
    private Controller controller; // miért a view része a kontroller?
    private JPanel panel;
    private JTextArea textArea;
    private JButton button;

    public JButton getButton() {
        return button;
    }

    public JTextArea getTextArea() {
        return textArea;
    }

    public void setTextArea(JTextArea textArea) {
        this.textArea = textArea;
    }
    
    //ide kerül a Jframe létrehozása. Ez az ablak addig tartja nyitva a programot, amíg be nem zárjuk.
    //lesz egy ablak, textArea fenn, alatta a gomb
    //gomnyomásra vagy az ActionListener interface-t kell felülírni, vagy lambdával megoldani egy sorban a következőt-
    //szóval ha megnyomta a gombot, akkor már a Controller lép működésbe, meghívunk egy metódust ott

    public SwingView(Controller controller) { //ezzel a konstruktorral hozzuk létre a view-n beül majd a controllert! Vagyis majd ez kell a konstruktorba: SwingView(Controller controller)
        this.controller=controller; //(később)
        textArea=new JTextArea("Action Event Logs:");
        button= new JButton ("Press it");
    }
    
    public void init(){
        setSize(500,500);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(textArea);
        add(button);
        setTitle("Swing Window");
        setLayout(new GridLayout(2, 1));
        button.addActionListener(ifPressed -> { 
        this.controller.logging(this); //ezt, mivel műveletet hajt végre, áthív a controller osztályba
        });     //ezért sírt itt, mert csak ebben az osztályban "megnevezett" változót, osztályt lehet itt definiálni? pl. controller 
    }
    
        public void modifyView (String text){
            textArea.append("\n"+text);
        }
        
    
}
