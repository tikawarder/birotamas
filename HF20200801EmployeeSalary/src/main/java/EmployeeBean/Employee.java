/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EmployeeBean;

import java.util.Objects;

/**
 *
 * @author tbiro
 */
public class Employee implements Comparable<Employee>{
    
    private String Name;
    private int age;
    private int salary;

    public Employee(String Name, int age, int salary) {
       
        this.Name = Name;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.Name);
        hash = 53 * hash + this.age;
        hash = 53 * hash + this.salary;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (this.age != other.age) {
            return false;
        }
        if (this.salary != other.salary) {
            return false;
        }
        if (!Objects.equals(this.Name, other.Name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Employee{" + "Name=" + Name + ", age=" + age + ", salary=" + salary + '}';
    }

    @Override
    public int compareTo(Employee o) {
        return this.getSalary()-o.getSalary();
    }
}
