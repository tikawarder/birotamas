/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200520swingrandombuttons;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class SwingWindow extends JFrame {
    static JButton[] numberButtons=new JButton[9];
    static JPanel panel;

    public SwingWindow()  {
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("Game");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panel = new JPanel(); //ez az alappanel
        GridBagLayout gbl= new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        panel.setLayout(gbl);
        add(panel);
        this.setVisible(true);
        setUpButtons();
    }
    
        public static void setUpButtons(){
        GridBagLayout gbl= new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        
        int num = 1;
        ActionListener numberListener = event->{
                    JButton jb = (JButton)event.getSource();
                    handleNumberButtonClick(jb.getText());//ide jön, hogy mit csináljon ha ráklikkelnek
                };
        for (int i = 3; i >=1; i--) {
            for (int j = 1; j < 4; j++) {
                JButton button = new JButton(Integer.toString(num));
                button.addActionListener(numberListener);
                gbc.gridx = j;
                gbc.gridy = i;
                gbc.weightx = 1;
                gbc.weighty = 1;
                gbc.gridwidth = 1;
                gbc.fill = GridBagConstraints.BOTH;
                Font f = new Font("Arial", Font.BOLD, 30);
                button.setFont(f);
                panel.add(button, gbc);
                numberButtons[num - 1] = button;
                num++;
            }
        }
        
}
    public static void handleNumberButtonClick (String buttonText){
        
        int pressedButtonIndex=Integer.parseInt(buttonText)-1;
        int rndIndex=pressedButtonIndex;
        while (rndIndex==pressedButtonIndex){
            rndIndex=(int)(Math.random()*9);
        }
        for (int index=0;index<9;index++){
            numberButtons[index].setText(Integer.toString(index+1));
        }
        numberButtons[rndIndex].setText("");
        
    }
}
    
    

