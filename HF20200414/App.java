package bh12animals;

import java.util.Random;

public class App {

    public static final Random rnd = new Random();

    public static void main(String[] args) {
        Creature[] creatures = new Creature[100];
        generateCreatures(creatures);
        
        int counter = 0;
      
        while(true) { //mindíg igaz lesz? Mikor áll le? Break-kel lép ki?
           Wolf attackerWolf=chooseAttackerWolf(creatures);
           Animal poorAnimal=choosePoorAnimal(creatures);        
            
            if(attackerWolf == null || poorAnimal == null) { // ez a ciklus egvizsgálja, hogy vagy a farkas, vagy az áldozat elfogyott-e
                System.out.println("counter: " + counter);
                break; // kilép a végtelen while ciklusból?
            }
            
            counter++;
            
            System.out.println(poorAnimal);//kiírja a farkas első áldozatát
            attackerWolf.attack();
            
            bites (poorAnimal, attackerWolf);          
        }
    }

    public static void generateCreatures(Creature[] creatures) {
        for (int i = 0; i < creatures.length; i++) {
            int rndNumber = rnd.nextInt(100);
            if (rndNumber < 60) {
                creatures[i] = new Chicken(rnd.nextInt(5));
            } else if (rndNumber < 65) {
                creatures[i] = new Turkey(rnd.nextInt(5));
            } else if (rndNumber < 70) {
                creatures[i] = new Cat(rnd.nextInt(10));
            } else if (rndNumber < 80) {
                creatures[i] = new Dog(rnd.nextInt(20));
            } else {
                creatures[i] = new Wolf(rnd.nextInt(20));
            }
        }

    }
    public static Wolf chooseAttackerWolf (Creature[] creatures){
         //elso elo farkas
            Wolf attackerWolf = null; // Wolf osztályú attackerwolf objektu, amely ekkor még üres.
            for (int i = 0; i < creatures.length; i++) {
                if (creatures[i] instanceof Wolf && creatures[i].isLive()) { //instanceof az megnézi, hogy pl wolf típusú-e a creture[i]? és hogy életben van-e
                    attackerWolf = (Wolf) creatures[i]; //objektumot wolf osztályúvá castol. Valójában itt példányosít egy támadó farkast, amivel később dolgozik.
                    break; //itt lép ki a for ciklusból, miután megvan az első élő farkas
                }
            }
        return attackerWolf;
    }
    public static Animal choosePoorAnimal (Creature[] creatures){
              //elso elo nem farkasCreature[] creatures
            Animal poorAnimal = null; //létrehoz egy üres animal objektumot
            for (int i = 0; i < creatures.length; i++) {
                if (!(creatures[i] instanceof Wolf) && creatures[i].isLive()) {
                    poorAnimal = (Animal) creatures[i]; //ez lesz az első megtámadott szegény állat, ami nem farkas
                    break; //kilép a for ciklusból, amint megvan az első élő nem farkas, akivel majd az attackerWolf összecsap
                }
            }
        return poorAnimal;
    }
    public static void bites (Animal poorAnimal, Wolf attackerWolf){
              //elso harapas
            poorAnimal.setLiveScore(poorAnimal.getLiveScore() - attackerWolf.getAttackScore());
            if (poorAnimal instanceof AttackInterface) {//megnezem, hogy az adott allat implementalja-e az attack interfacet // vagyis vissza tud támadni, harapni
                AttackInterface ai = (AttackInterface) poorAnimal; // interface típusú ai változó??? objektum? Vagy castolás
                boolean bothAlive = true;
                while (bothAlive) {
                    ai.attack();
                    attackerWolf.setLiveScore(attackerWolf.getLiveScore() - ai.getAttackScore()); //ennyivel csökkenti a visszatámadó állat a farkas életerejét. Vagyis erre jó az ai?

                    if (attackerWolf.isLive()) { //ha életben maradt a farkas
                        attackerWolf.attack();
                        poorAnimal.setLiveScore(poorAnimal.getLiveScore() - attackerWolf.getAttackScore());
                    }
                    bothAlive = attackerWolf.isLive() && poorAnimal.isLive();
                }
                
                Dog.underAttack = false;
                
            }
        }
    }



/*
Legyen egy kert, ahol vannak állatok, 100 db, csirke, macska stb.
Legyenek vadállatok, pl. farkas
minden állanak legyen hp-je, vadállatoknak damage

vadállatok betörnek a kertbe, próbálnak legyőzni háziállatokat
 */
