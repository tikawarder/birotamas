/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banksimulation.dataload;

import banksimulation.BankApplication;
import banksimulation.model.Bank;
import banksimulation.model.Client;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author tbiro
 */
public class DataWriter {

    public void writeClientsData(Bank bank) {

        File clientFile = new File(bank.toString()+"_allclients.txt");

        try (FileWriter fw = new FileWriter(clientFile);
                BufferedWriter bw = new BufferedWriter(fw);) {
            
            StringBuilder sb = new StringBuilder();
            //sb.append("Client_Id Account_number Balance" );
            //sb.append(System.lineSeparator());
            for (Client c : bank.getClients()) { 
                sb.append(c.getClientId() + " ");
                sb.append(c.getAccountNumber() + " ");
                sb.append(c.getBalance());
                sb.append(System.lineSeparator());
            }
            bw.write(sb.toString());

        } catch (IOException e) {
            System.out.println("Hiba a fájl írása közben! :(");
        }
    }

}
