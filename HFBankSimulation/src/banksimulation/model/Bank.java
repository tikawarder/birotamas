/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banksimulation.model;

import java.util.ArrayList;
import java.util.List;

public class Bank {
    private String name;
    private List<Client> clients = new ArrayList<>();
    private List<Transfer> transfers = new ArrayList<>();

    public Bank(String name) {
        this.name = name;
    }

    public  List<Client> getClients() {
        return clients;
    }

    public  List<Transfer> getTransfers() {
        return transfers;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public void setTransfers(List<Transfer> transfers) {
        this.transfers = transfers;
    }

    @Override
    public String toString() {
        return name;
    }

}
