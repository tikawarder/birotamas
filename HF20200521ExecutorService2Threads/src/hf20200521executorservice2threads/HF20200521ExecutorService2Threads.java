/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200521executorservice2threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



public class HF20200521ExecutorService2Threads {

 
 
    public static void main(String[] args) {
        ExecutorService es = Executors.newFixedThreadPool(2);    
        ScannerThread scanner = new ScannerThread();
        Printer print = new Printer();
        es.submit(scanner);
        es.submit(print);
        //es.shutdownNow() printer;//nem értem, hogyan lehetne leállítani a printer ágat egy másik ágból
}
}
