package carrental;

import carrental.model.Car;
import carrental.model.Company;
import carrental.model.DieselCar;
import carrental.model.ElectricCar;
import carrental.model.PetrolCar;
import carrental.model.Site;
import java.util.List;
import java.util.Scanner;

public class Application {

    public static Scanner sc = new Scanner(System.in);

    public static final int DEFAULT_SITE_NUMBER = 3;
    public static final int DEFAULT_CAR_NUMBER = 6;

    public static int siteCounter = 0; //később használja a site-ok léptetéséhez
    public static int kmLimitforPrint=150;
            
    public static void main(String[] args) { //ide kerül a program
        Company avis = new Company(); //példányosítja a céget, létrehoz egyet Avis néven

        initSites(avis); //metódus a site-ok létehozásához

        System.out.println("Kérem válasszon menüpontot:");
        System.out.println("1: Telephelyek listázása");
        System.out.println("2: Adott telephely listázása");
        System.out.println("3. Adott telephelyen kölcsönözhető autók listázása, amelyek 150 km-nél többet meg tudnak tenni a jelenlegi tankjukkal");
        System.out.println("4: A cégnél lévő autómárkák kölcsönzési árainak listázása");
        System.out.println("5: Konkrét kölcsönzés telephely és autó alapján");
        int option = sc.nextInt();

        switch (option) {
            case 1:
                printSites(avis);
                break;
            case 2:
                printCarsForSite(avis,0);
                break;
            case 3:
                printCarsForSite(avis,kmLimitforPrint);
                break;
            case 4:
                printCarsWithPrices(avis);             
            case 5:
                rental(avis);
            default:
                System.out.println("Köszi, ennyi volt az alkalmazás.");
        }
    }

    public static void printSites(Company c) {
        List<Site> telepHelyGyujto = c.getSites(); //egy üres listába lekéri getterrel az adott cég telephelyeit
        for (Site iterátor : telepHelyGyujto) {
            System.out.println(iterátor.getSiteId() + "|" + iterátor.getAddress());
        }
    }

    public static void initSites(Company tempCompany) {
        List<Site> lokalisLista = tempCompany.getSites(); //még csak referencia. lokáis listát hoz létre (csak a metódusban) .getter lekéri, hogy milyen siteok vannak a cégnél
        for (int i = 0; i < DEFAULT_SITE_NUMBER; i++) {
            lokalisLista.add(generateRandomSite()); //mindíg a legfontosabb: ha megvan az adat metódusokkal, akkor hozzá kell Adni ADD-dal
            //mihez.mit(ezt adjuk hozzá) = listanév.parancs(mit)
        }
    }

    private static Site generateRandomSite() {
        int i = (int) (Math.random() * 10);
        Site adottTelephely = new Site(); //referencia Objektum, csak létrehoz, lefoglal memóriában, megnevez

        adottTelephely.setSiteId(siteCounter); //ad a site-nak egy id számot setterrel
        siteCounter++;

        adottTelephely.setAddress("Nagy László út " + i * 13); //beállítja setterrel a cíímet

        List<Car> adottTelepHelyAutoListaja = adottTelephely.getCars(); //létrehoz egy listát referencisként az adott telephelyen. Belerakja a tlephelyi utolistát

        for (int j = 0; j < DEFAULT_CAR_NUMBER; j++) {
        
                    Car rndCar = generateRandomCar(); //nem ad paramétert az auto létrehozáshoz! de visszakap egy véletlenszerűen generált autót
                    adottTelepHelyAutoListaja.add(rndCar); //ez adja hozzá ADD-dal a telephelyi listához egyenként a kocsikat
            }

            return adottTelephely;
        }

    

    private static Car generateRandomCar() { // ez véletlenszerűen létrehoz egy autót a tulajdonságaival és visszaadja lokalisCar-ként!
        int i = (int) (Math.random() * 10);
        int j = (int) (Math.random() * 10);
        Car lokalisCar; 
        
        int carType = (int) (Math.random() * 3 + 1);
        
            switch (carType) {
                case 1:
                    lokalisCar = new ElectricCar(Math.random()); //Adok neki charge levelt construktorként , demiért nem megy setCharge level? Mert előtte nem volt konstruktor
                    break;
                case 2:
                    lokalisCar = new PetrolCar();
                    break;
                default:
                    lokalisCar = new DieselCar();
            }
        
        lokalisCar.setColor("Color" + j); //setterekkel beállítja a változóit
        lokalisCar.setDoorCount(j);
        lokalisCar.setKilometerCount(j * 1000);
        lokalisCar.setPlateNumber(null);
        lokalisCar.setModel("Model" + j);
        lokalisCar.setYearOfCreation((i + j) * 1000);
        lokalisCar.setActualPetrolLevel(55);
        lokalisCar.setRentalPrice(i*j*50);
        System.out.println(lokalisCar.getClass()+"modell: "+lokalisCar.getModel()+ " Ennyi km-t tud megtenni a kocsi: " + lokalisCar.kmCalc()); //próba kiíratás
        return lokalisCar; //FONTOS! visszaadja a kész referencia objektumot
    }

    private static Site printCarsForSite(Company c, int limit) {
        System.out.println("Melyik telephely autóid szeretné listázni?");
        int melyikSiteID = sc.nextInt();

        List<Site> sites = c.getSites(); //még üres lista a cég összes site-jairól

        Site aKeresettTelephely = new Site(); //referencia site objektum létrehozása

        for (Site s : sites) { //végéignézi a telephelylistát
            if (s.getSiteId() == melyikSiteID) { //egyenként lekéri a telephelyID-kat, megnézi a bekért ID-val való eggyezőségét
                aKeresettTelephely = s; //elmenti hogy a keresett iD melyik telephelyhez tartozik
            }
        }

        List<Car> lekertAutoLista = aKeresettTelephely.getCars(); // a megtalált telephelynek lekéri egy új listába az autóit

        for (Car car : lekertAutoLista) { //végigIterál a megtalált listán és kiírja a tagjait (az autókat)
            if (car.kmCalc()>limit) System.out.println(car); //lehet elegánsabb, ha a istába bele sem kerülne?        
        }
        return aKeresettTelephely;
    }
    private static void printCarsWithPrices (Company c){
        List<Site> sites = c.getSites();
        
        for (Site s :  sites) {
            List<Car> lekertAutoLista = s.getCars(); // telephelynek lekéri egy új listába az autóit
            System.out.println(s);
            for (Car kocsik: lekertAutoLista){
                System.out.println("Auto: "+kocsik.getModel()+" Kölcsönzési ár: "+kocsik.getRentalPrice());
            }
    }
    }
    private static void rental (Company c){
        Site telephelyId=printCarsForSite(c,0);
        Car keresettKocsi = null;
        System.out.println("Melyik modellt szeretnéd ezek közzül?");
        String model = sc.next();
        List<Car> lekertAutoLista = telephelyId.getCars();
        for (Car kocsik : lekertAutoLista) { //végéignézi a kocsilistát
            if (kocsik.getModel().equals(model)) { //egyenként lekéri a kocsikat, megnézi a bekért modellel való eggyezőségét
                keresettKocsi = kocsik; //elmenti 
            }
            else {
                System.out.println("Sajnos nincs ilyen autó!");
                return;
            }
        }
        System.out.println("Mennyi km-t tervezel menni?");
        int plannedKm = sc.nextInt();
        if (keresettKocsi.kmCalc()<plannedKm) {
            System.out.println("Nem tudsz ennnyi km-t menni ezzel a kocsival 1 nap alatt!");
        }
        else {
            System.out.println("Rendben, a kolcsönzést rögzítettem!");
         keresettKocsi.setIsRented(true);
        }

    }
    
}
