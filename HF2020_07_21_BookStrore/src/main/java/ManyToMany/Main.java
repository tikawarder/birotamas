/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManyToMany;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
        public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpamain");
        EntityManager em = factory.createEntityManager();
        
        List<Student> students = em.createQuery("select s from Student s").getResultList();
        
        for (Student s : students) {
            System.out.println(s.getName() + " a következő tárgyakat szereti:");
            for (Course c : s.getCourses()) {
                System.out.println(c.getName());
            }
        }
    }

}