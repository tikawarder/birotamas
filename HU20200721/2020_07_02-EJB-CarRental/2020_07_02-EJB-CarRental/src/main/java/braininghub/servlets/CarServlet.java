package braininghub.servlets;

import braininghub.dao.Cars;
import braininghub.dao.CarDAO;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/CarServlet")
public class CarServlet extends HttpServlet {
    
    @Inject //ettől már Bean lesz!
    public CarDAO carDao;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("car.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        int carId = Integer.valueOf(req.getParameter("carId"));
        String details = req.getParameter("details");
        System.out.println(carId +","+details);
        //carDAO.addCar(carId, details); //ez most akkor mihez adja hozzá?
        Cars newCar = new Cars();
        newCar.setCarId(carId);
        newCar.setDetails(details);
        
        System.out.println(newCar);
        
        CarDAO carDao = new CarDAO(newCar);
        carDao.addCar(newCar);
        
          /*
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("carrental"); //ua. mint a @PersistenceContext
        EntityManager em = factory.createEntityManager();
        em.persist(newCar);
        
        //em.createQuery("insert into carDAO (CarId,Details) values (1,\"Jókocsi\")").getResultList();
        
      
        List<CarDAO> carDTOList = em.createQuery("select s from CarDAO s").getResultList(); //fontos a getResult...mertcsak ekkor fut le!

        for (CarDAO s: carDTOList){
            System.out.println(s);
        }
        */
        
        res.sendRedirect("MainMenuServlet");
    }
}
