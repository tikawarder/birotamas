/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPAMain;

import Entities.Car;
import Entities.Company;
import Entities.Site;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
    public static void main(String[] args) { //itt a mainben csak futtatunk queryket
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpamain"); //ua. mint a @PersistenceContext
        EntityManager em = factory.createEntityManager(); //ez idáig csinál valamit, vag ycsak előkészít?
        
       
        List<Car> listCar = em.createQuery("select c from Car c").getResultList(); //fontos a getResult...mertcsak ekkor fut le!       
        for (Car c: listCar){
            System.out.println(c);
            System.out.println(c.getSite().getAddress());
        }
        
        
        List<Site> siteList = em.createQuery("select s from Site s").getResultList(); //fontos a getResult...mertcsak ekkor fut le!

        for (Site s: siteList){
            System.out.println(s);
        }
        
        /*
        List<Company> companyList = em.createQuery("select s from Company s").getResultList(); //fontos a getResult...mertcsak ekkor fut le!

        for (Company s: companyList){
            System.out.println(s);
        }
        */
    }
}
