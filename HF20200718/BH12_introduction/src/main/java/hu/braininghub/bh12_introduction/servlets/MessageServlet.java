package hu.braininghub.bh12_introduction.servlets;

import hu.braininghub.bh12_introduction.model.MessageDTO;
import hu.braininghub.bh12_introduction.service.MessageService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "MessageServlet", urlPatterns = {"/sendMessage"}) //kérdés:az utlPatterns alapján lehet hivatkozni rá formban?
public class MessageServlet extends HttpServlet {
    
    @Inject //ettől már Bean lesz!
    private MessageService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        session.setAttribute("sentMessage", Boolean.TRUE); //beállítja a sentMessage attributot TRUE-ra!
        //session.setAttribute("messages", service.getMessages());
        
        String email = request.getParameter("email"); //name vagy id alapján?
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");

        service.addMessage(email, subject, message); //előtte injektáltunk egy service-t, a MessageService osztályból
        //System.out.println(request.getContextPath());
        response.sendRedirect(request.getContextPath() + "/index.jsp");//csinal egy uj http kerest
        //request.getRequestDispatcher("index.jsp").forward(request, response);//nem csinal uj http kerest
        //System.out.println(service.getMessages().size());//masodik esetben folyamatosan novekedni fog a message szam        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
