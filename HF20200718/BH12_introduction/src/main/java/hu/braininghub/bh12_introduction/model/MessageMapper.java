/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh12_introduction.model;

/**
 *
 * @author tbiro
 */
public class MessageMapper { //átmappalés: egyik típusról másikra, azért hogy ne jusson ki személyes adatbázis adat

    public static MessageDTO toDTO(Message message) {
        MessageDTO dto = new MessageDTO();
        dto.setEmail(message.getEmail());
        dto.setMessage(message.getMessage());
        dto.setSubject(message.getSubject());
        dto.setId(message.getId());

        return dto;
    }
}

