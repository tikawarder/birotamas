/*
 2. generáljunk egy tömböt véletlen számokkal, majd írjuk ki azt a számot, amikortól kezdve tőle csak nagyobb szám van a tömbben. 
pl. 
3, 4, 7, 3, 4, 6 esetén a 3 a megoldás
9, 3, 1, 5, 2, 4, 6 esetén a 2 a megoldás
 */
package hf20200328_2;

public class HF20200328_2 {
    public static int MIN=10,MAX=99,ARRAY_SIZE=10;

    public static void main(String[] args) {
        int[] array= randomFillArray(MIN,MAX,ARRAY_SIZE);
        checkAndPrint(array);
    }
    public static int[] randomFillArray (int MIN,int MAX,int ARRAY_SIZE){
        int [] array=new int[ARRAY_SIZE];
        for (int i=0;i<ARRAY_SIZE;i++){
            array[i]=(int)(Math.random()*(MAX-MIN)+MIN);          
        }
        return array;
    }
    public static void checkAndPrint(int[]array){
        int getNumber=0;
        System.out.println("A tömb adatai:");
        for (int i=0;i<ARRAY_SIZE-1;i++){
            System.out.print(array[i]+" ");
                if (array[i+1]<array[i]) getNumber=i+1;
        }
        System.out.print(array[ARRAY_SIZE-1]);
        System.out.println("\nA keresett szám: "+array[getNumber]);
    }
    
}
