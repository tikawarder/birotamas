/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingbookreader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;


/**
 *
 * @author tbiro
 */
public class FileChoose {
    
    private String location;
    private Book book;

    public FileChoose(String location) {
        this.location = location;
        this.book=fileChooser(location);
    }

    public Book getBook() {
        return book;
    }

    
public Book fileChooser (String location){
    JFileChooser fileOpener = new JFileChooser(location); //ez az elérési utat kell majd paraméterként meghívni
        fileOpener.showOpenDialog(null);
        File selectedFile=fileOpener.getSelectedFile();
        StringBuilder fileName=new StringBuilder(selectedFile.getName());
        String endOfFile=fileName.substring(fileName.length()-4,fileName.length()); //kiszedi az utolsó 4 karaktert
        
        if (endOfFile.equals(".txt"))  {
            System.out.println("Ez egy text file");
            return txtFileReader (selectedFile);
        }
        else if (endOfFile.equals(".ser")) {
            System.out.println("Ez egy .ser file");
            return readSerialisedFile (selectedFile);
        }
            System.out.println("Nem tudok beolvasni ilyen file-t, próbálj .ser vagy .txt kiterjesztést!");
            Book book = new Book("empty");
            return book;    
    }
public Book txtFileReader (File selectedFile){
        Book book= new Book("txt");
        String line=null;
        try (java.io.FileReader fr= new java.io.FileReader(selectedFile);
                BufferedReader br= new BufferedReader(fr)){      
        line=br.readLine();
        /* //ez majd többsoros állományhoz lesz jó
        while (line!=null){
            lines.add(line);
            line=br.readLine();
        }
        */
        book.setText(line);
    }   catch (IOException ex) {
            System.err.println(ex.getStackTrace()+" Hiba file elérése közben!"); //System.err kell
        }
        return book;
        }
        
    public Book readSerialisedFile (File file){ //.ser file beolvasás
        Book book= new Book("ser");
        try (FileInputStream fis = new FileInputStream(file);
                MyGenericInputStreamReader<Book> ois = new MyGenericInputStreamReader<>(fis)) { //ide miért kellett egy speciális reader osztályt létrehozni?
        
        book = ois.readObjectGeneric();
        
    } catch (FileNotFoundException ex) {
        Logger.getLogger(SwingBookReader.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
        Logger.getLogger(SwingBookReader.class.getName()).log(Level.SEVERE, null, ex);
    } catch (ClassNotFoundException ex) {
            Logger.getLogger(SwingBookReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    return book;
    }
}
