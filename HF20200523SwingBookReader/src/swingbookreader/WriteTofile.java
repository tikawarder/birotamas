/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingbookreader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author tbiro
 */
public class WriteTofile {
    //private String link;
    private File file;
    private String textToWrite;

    public WriteTofile(String link, String textToWrite) {
        this.file = new File(link);
        this.textToWrite = textToWrite;
        writeToFile (file,textToWrite);
    }
    
    public void writeToFile (File file, String textTOWrite){
       try (FileWriter fw= new FileWriter(file);
                BufferedWriter br= new BufferedWriter(fw)){
          br.write(textTOWrite);
           
       } catch (IOException ex) {
            System.err.println("Hiba a file írása közben!"); 
        }
    }
}
