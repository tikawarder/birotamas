/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingbookreader;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public class MyGenericInputStreamReader<T> extends ObjectInputStream { //csinál egy speciális beolvasó osztályt

    public MyGenericInputStreamReader(InputStream in) throws IOException { ///konstruktor
        super(in);
    }

    public MyGenericInputStreamReader() throws IOException, SecurityException { //hibát dobhat
    }

    public T readObjectGeneric() throws IOException, ClassNotFoundException { //metódus, ami meghívja a readObject metódust
        return (T) readObject();
    }
}
