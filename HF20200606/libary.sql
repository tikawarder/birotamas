show databases; /* megadja az elérhető db-esek */

CREATE DATABASE IF NOT EXISTS libary;
USE libary;
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS visitors;
DROP TABLE IF EXISTS rentals;


CREATE TABLE IF NOT EXISTS authors (
	id INT AUTO_INCREMENT PRIMARY KEY,
    book_reference_id INT,
	name VARCHAR(256),
	country VARCHAR(128)
);

CREATE TABLE IF NOT EXISTS categories (
	id INT AUTO_INCREMENT PRIMARY KEY,
	category VARCHAR(256)
);


CREATE TABLE IF NOT EXISTS books (
	id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(512) NOT NULL,
    year_of_issue INT,
    description TEXT,
    author_id INT,
    category_id INT, /*category VARCHAR(128),*/
    rentable BIT,
    max_rent_time VARCHAR(128),
    FOREIGN KEY (author_id) references authors(id),
    FOREIGN KEY (category_id) references categories(id)
);

 CREATE TABLE IF NOT EXISTS visitors (
	id INT,
    name varchar(128),
    birthDate date,
    address varchar(255)
);
CREATE TABLE IF NOT EXISTS rentals (
	id int,
    book_id int,
    rental_date date
);

/*SHOW TABLES;*/

/*DESCRIBE books;*/

INSERT INTO authors (name,country) VALUES ('Jonas Jonasson','Swedish');
INSERT INTO authors (name,country) VALUES ('Robert C. Martin','English');

INSERT INTO categories (id,category)
VALUES (1,'vijatek');

INSERT INTO books (title,year_of_issue,description,author_id,category_id,rentable,max_rent_time)
VALUES ('szaz eves ember',2009,'kiraly könyv',1,1,1,'2 het');

select * from books;