/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing4buttons;

import java.util.List;
import java.io.File;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author tbiro
 */
public class Model {
    
    private int logCounter;
    private List<Employee> list= new ArrayList<>();
    private File file = new File("logs.txt");
    private Map<Integer, LocalTime> logMap = new TreeMap<>();
    

    public int getLogCounter() {
        return logCounter;
    }

    public Map<Integer, LocalTime> getLogMap() {
        return logMap;
    }

    public void setLogMap(Integer integer, LocalTime time) {
        this.logMap.put(integer, time);
    }

    public File getFile() {
        return file;
    }

    public void setLogCounter(int logCounter) {
        this.logCounter = logCounter;
    }

    public void addToList(Employee emp) {
     this.list.add(emp);
    }

    public void setList(List<Employee> list) {
        this.list = list;
    }

    public List<Employee> getList() {
        return list;
    }

}
