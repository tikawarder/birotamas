package zhfeladataruhaz;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tbiro
 */
public class Entertainment extends Products implements SwitchAble {
    private int swithes;
 
    public int getSwithes() {
        return swithes;
    }

    public Entertainment(Enum properties, String manufacturer, int barCode, int price) {
        super(properties, manufacturer, barCode, price);
        this.swithes=0;
    }

    public Entertainment() {
    }

    public void setSwithes(int swithes) {
        this.swithes = swithes;
    }
 
    @Override
    public void switchOn() throws SpecialFauilureEx { //ez a metódus dobhat hibát. Dob is, ha 5-nél nagyobb a switch. Hol kezeljük?
        this.swithes++;
        if (swithes>5) {
            throw new SpecialFauilureEx(this.getBarCode()+" vonalkód 5 bekapcsolás után elromlott!");
        }
    }
}
