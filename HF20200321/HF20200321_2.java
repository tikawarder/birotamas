/*
2. A program töltsön fel véletlenszerűen kétjegyű számokkal egy 500 elemű tömböt, majd csináljon statisztikát róla, melyik szám hányszor fordul elő!
 */
package hf20200321_2;

/**
 *
 * @author tbiro
 */
public class HF20200321_2 {

    public static void main(String[] args) {
        int min=10;
        int max=99;
        int arraySize=500;
        int[] array = fillWithRandoms(arraySize, min, max);
        numberStat(array,min,max); 
    }
    
    public static int[] fillWithRandoms(int size, int min, int max) {
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * (max+1 - min) + min);
        }
        return array;
    }
    public static void numberStat(int[]array,int from, int to){
        int [] howMany= new int [to-from+1];
        for (int i = 0; i < array.length; i++) {
            howMany[array[i]-from]++;
        }
        for (int i=0;i<howMany.length;i++){
            System.out.println(i+from+". szám "+howMany[i]+"x található meg!");
        }
    }
}
