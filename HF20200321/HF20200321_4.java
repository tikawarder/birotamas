/*
4. A program legyen képes ötös és hatos lottó egy lehetséges húzásának eredményét visszaadni. A program köszöntse a felhasználót, majd egy menüvel döntse el, hogy ötös vagy hatos lottó számokat adjon vissza (menü) 
 */
package hf20200321_4;

import java.util.Scanner;

public class HF20200321_4 {

    public static void main(String[] args) {
        String name = "Tamás";
        greating(name);
        System.out.println("MENU");
        System.out.println("1.ötöslottó sorsolás");
        System.out.println("2.hatoslottó húzás");
        Scanner sc = new Scanner(System.in);
        int menu;
        do {
            System.out.println("\n Válassz a menüből!");
            menu = sc.nextInt();
        } while (menu < 1 || menu > 2);
        switch (menu) {
            case 1:
                lottery(90, 5);
                break;
            case 2:
                lottery(45, 6);
        }
    }

    public static void greating(String name) {
        System.out.println("Szép napot Kedves " + name + "! \n");
    }

    public static void lottery(int max, int howMany) {
        int[] lottery = new int[howMany + 1];
        boolean equal;
        System.out.println("At 5-ös lottó kisorsolt számai:");
        for (int nr = 1; nr <= howMany; nr++) {
            do {
                lottery[nr] = (int) (Math.random() * max + 1);
                equal = false;
                for (int i = 0; i < nr; i++) {
                    if (lottery[nr] == lottery[i]) {
                        equal = true;
                    }
                }
            } while (equal == true);
            System.out.println(nr + ".ik szám: " + lottery[nr]);
        }
    }

}
