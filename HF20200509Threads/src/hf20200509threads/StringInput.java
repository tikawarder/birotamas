/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200509threads;

import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class StringInput extends Thread {
    
    private Queue<String> list = new PriorityQueue<>();
    private Scanner sc = new Scanner(System.in);
    boolean isEnd;

    public StringInput(Queue<String> list){
        this.list=list;
    }
    
    public boolean isIsEnd() {
        return isEnd;
    }

    @Override
    public void run() {
        while (true) {
            System.out.println("Adj meg sorokat!");
            String input = sc.nextLine();
            synchronized (list) {
                list.add(input);
            }
            if (input.equals("")) {
                break;
            }
        }
    }

}
