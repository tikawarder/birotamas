/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200509threads;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueWriter extends Thread {
    
    private Queue<String> list = new PriorityQueue<>(); 
    private boolean isEnd;
    private int writtenLines;
    private int size;
    private boolean isEmpty;

    public QueueWriter (Queue<String> list){
        this.list=list;
    }
    
    @Override
    public void run() {
        writtenLines=0;
        while (isEnd == false) {
            //System.out.println("csak várakozok, hogy változzon a lista mérete!");
            synchronized (list) {
                size=list.size();
                if (list.isEmpty()==true) size=0;
            }
            
            while (writtenLines != size) {
                StringBuilder sb = new StringBuilder();
                File file = new File("queue.txt");

                try (FileWriter fw = new FileWriter(file);
                        BufferedWriter bw = new BufferedWriter(fw);) {
                    synchronized (list) {
                    for (String line : list) {
                        if (line.equals("")) {
                            isEnd = true;
                        }
                        sb.append(line);
                        sb.append(System.getProperty("line.separator")); //vagy kipróbálni az .append("\n")
                    }
                    }
                    bw.write(sb.toString());
                    synchronized (list) {
                    writtenLines = list.size();
                    }
                    System.out.println("Most írtam file-ba!");
                } catch (IOException e) {
                    System.out.println("Hiba a file írása közben!");
                }

            }
        }

    }
}
