/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf20200407fruits;

public class Apple extends Fruit {

    private String taste;

    public Apple(String color, double price, String taste) {
        super(color, price);
        this.taste = taste;
    }

    public String getTaste() {
        return taste;
    }

}
